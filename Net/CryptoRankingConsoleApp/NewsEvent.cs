﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataOrganizerLibrary
{
    public class PublicFigure
    {
        public string FirstName;
        public string SecondName;
        public List<string> Roles;
    }

    public class EventData
    {
        public double _actual;
        public double _forecast;
        public double _previous;
        private string _numericType;
    }

    public class NewsEnity
    {
        public string Author;
        public string Title;
        public string Description;

        public string BaseTitle;
        public string BaseDescription;

        public string Url;
        public string UrlImage;
        public string NewsSource;
        public DateTime PublishDateTime;
        public List<DateTime> PotentialEventDateTimes = new List<DateTime>();

        public List<KeyValuePair<string, string>> NER_Title = new List<KeyValuePair<string, string>>();
        public List<KeyValuePair<string, string>> NER_Description = new List<KeyValuePair<string, string>>();
        public List<KeyValuePair<string, string>> CommonAttributes = new List<KeyValuePair<string, string>>();

        public List<List<string>> DescriptionTextCombos = new List<List<string>>();
        public List<List<string>> DescriptionTextCombosBaseTypes = new List<List<string>>();


        public Dictionary<string, int> WordCount = new Dictionary<string, int>();
        public List<KeyValuePair<string, int>> CommonScoredWords = new List<KeyValuePair<string, int>>();
    }

    public class NewsEvent
    {
        private DateTime _releaseDateTime;
        private double _actual;
        private double _forecast;
        private double _previous;

        private string _eventHeadline;
        private string _eventType;

        private string _eventDescription;
        private List<string> _category = new List<string>();
        private string _institutionBody;
        private string _currency;
        private List<string> _country = new List<string>();

        private List<string> _sources = new List<string>();
        private string _primarySource;


        private string _numericType;
        private string _importance;
        private string _associatedSymbolID;

        private List<PublicFigure> _associatedPublicFigures = new List<PublicFigure>();

        public DateTime ReleaseDateTime
        {
            get { return _releaseDateTime; }
            set { _releaseDateTime = value; }
        }

        public double Actual
        {
            get { return _actual; }
            set { _actual = value; }
        }

        public double Forecast
        {
            get { return _forecast; }
            set { _forecast = value; }
        }

        public double Previous
        {
            get { return _previous; }
            set { _previous = value; }
        }

        public string EventHeadline
        {
            get { return _eventHeadline; }
            set { _eventHeadline = value; }
        }

        public string EventType
        {
            get { return _eventType; }
            set { _eventType = value; }
        }
        public string EventDescription
        {
            get { return _eventDescription; }
            set { _eventDescription = value; }
        }

        public List<string> Category
        {
            get { return _category; }
            set { _category = value; }
        }

        public string InstitutionBody
        {
            get { return _institutionBody; }
            set { _institutionBody = value; }
        }

        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }

        public List<string> Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public string NumericType
        {
            get { return _numericType; }
            set { _numericType = value; }
        }

        public string Importance
        {
            get { return _importance; }
            set { _importance = value; }
        }

        public string AssociatedSymbolID
        {
            get { return _associatedSymbolID; }
            set { _associatedSymbolID = value; }
        }

        public List<PublicFigure> AssociatedPublicFigures
        {
            get { return _associatedPublicFigures; }
            set { _associatedPublicFigures = value; }
        }
        
        public List<string> Sources 
        {
            get { return _sources; }
            set { _sources = value; }
        }

        public string PrimarySource
        {
            get { return _primarySource; }
            set { _primarySource = value; }
        }
    }
}
