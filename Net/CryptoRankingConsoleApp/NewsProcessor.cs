﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using CryptoRankingConsoleApp;
using HtmlAgilityPack;
using System.Net;
using System.Text;

namespace DataOrganizerLibrary
{
    public class EventsCorona
    {
        public string Province { get; set; }
        public string Country { get; set; }
        public int Deaths { get; set; }
        public int DeathsTillDate { get; set; }
        public string Region { get; set; }
        public string SubRegion { get; set; }
        public string ISO { get; set; }
        public string Long { get; set; }
        public string Lat { get; set; }
        public List<DateTime> EventDate { get; set; }
    }


    public class NewsProcessor
    {
        private List<string> _newsAgencies = new List<string>();

        private Dictionary<string, string> _logos = new Dictionary<string, string>();

        public NewsProcessor()
        {
            NewsListings();
        }
      
        //public Dictionary<int, List<NewsEnity>> GetLiveNews()
        //{
        //     var newsCollection = FetchNews();
        //    //var newsCollection = Diagnostic_ReadCSVFile();

        //    //group events by headline and contents/ similarities
        //    return GroupNewsArticlesBySimilarities(newsCollection);
        //}


        public List<CryptoRank> FetchRankingAPI()
        {
            var newsAPI = System.Configuration.ConfigurationManager.AppSettings["NEWSAPI"].ToString();
            var limit = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SNAPSHOT_LIMIT"].ToString());

            var restClient = new RestClient();
            var newsCollection = new List<CryptoRank>();

            try
            {
                var url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?sort=percent_change_24h&sort_dir=desc&limit=5000&cryptocurrency_type=coins&circulating_supply_min=50000000&market_cap_min=200000000";

                var result = restClient.GetFromRestService(url);
                newsCollection.AddRange(ConvertToNewsEntity(result, ""));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return newsCollection.OrderByDescending(m => m.Percentage24h).Take(limit).ToList();
        }

        public List<CryptoRank> FetchRankingScrap()
        {
            var newsAPI = System.Configuration.ConfigurationManager.AppSettings["NEWSAPI"].ToString();
            var limit = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SNAPSHOT_LIMIT"].ToString());

            var restClient = new RestClient();
            var newsCollection = new List<CryptoRank>();

            try
            {
                var url = "https://coinmarketcap.com/";

                newsCollection.AddRange(ConvertToCryptoRankEntity(url));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return newsCollection.OrderByDescending(m => m.Percentage24h).Take(limit).ToList();
        }

        public List<CryptoRank> FetchRankingMain()
        {
            var result = FetchRankingScrap();

            if (result == null || result.Any() == false)
            {
                result = FetchRankingAPI();
            }

            return result;
        }

        private List<CryptoRank> ConvertToCryptoRankEntity(string source)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;

            var getHtmlWeb = new HtmlWeb();

            string classToFind = "__NEXT_DATA__";

            try
            {
                HtmlDocument document = getHtmlWeb.Load(source);
                Console.WriteLine(source);

                var eventHeadline =
                    document.DocumentNode.SelectNodes(string.Format("//*[contains(@id,'{0}')]", classToFind));

                dynamic json = Newtonsoft.Json.JsonConvert.DeserializeObject(eventHeadline.FirstOrDefault().InnerText);
                var all = json["props"]["initialState"]["cryptocurrency"]["listingLatest"]["data"];
                var resultsCount = (System.Collections.IList)all;

                if (resultsCount != null)
                {
                    var newEntityListConcurrent = new System.Collections.Concurrent.ConcurrentBag<CryptoRank>();
                    int[] coll = new int[resultsCount.Count];
                    for (int i = 0; i < resultsCount.Count; i++) { coll[i] = i; }

                    Parallel.ForEach(coll,
                        i =>
                        {
                            var newEntity = new CryptoRank();
                            newEntity.Name = all[i]["name"];
                            newEntity.Symbol = all[i]["symbol"] + "USDT";

                            string sym = all[i]["symbol"];

                            if (_logos.TryGetValue(sym, out var link))
                            {
                                newEntity.Logo = link;
                            }
                            newEntity.MarketCap = Math.Round(Convert.ToDouble(all[i]["quote"]["USD"]["marketCap"]), 2);

                            newEntity.Percentage1h = Math.Round(Convert.ToDouble(all[i]["quote"]["USD"]["percentChange1h"]), 2);
                            newEntity.Percentage24h = Math.Round(Convert.ToDouble(all[i]["quote"]["USD"]["percentChange24h"]), 2);


                            newEntity.Percentage7d = Math.Round(Convert.ToDouble(all[i]["quote"]["USD"]["percentChange7d"]), 2);

                            newEntity.Price = Math.Round(Convert.ToDouble(all[i]["quote"]["USD"]["price"]), 4);

                            newEntity.CirculatingSupply = Convert.ToDouble(all[i]["circulatingSupply"]);

                            newEntity.Volume = Math.Round(Convert.ToDouble(all[i]["quote"]["USD"]["volume24h"]), 2);

                            newEntityListConcurrent.Add(newEntity);
                        });

                    return newEntityListConcurrent.ToList();
                }
                var game = document.DocumentNode.SelectNodes("//table//tr");

            }
            catch (Exception ex)
            {
                //no need api will take over

                //var message = new StringBuilder();
                //message.AppendLine("Coinmarket Cap page parsing failed. Update all CryptoRank Scrappers");
                //message.AppendLine("Current Criteria :- " + classToFind);
                //message.AppendLine("");
                //message.AppendLine(ex.ToString());

                //new EmailManager().Send(null, "Coinmarket Cap Scraping Failure", message.ToString());
            }

            return null;
        }



        //public List<EventsCorona> CoronaData_ReadCSVFile()
        //{
        //    var collect = new List<EventsCorona>();

        //    var collectEvent = new List<EventsCorona>();
        //    try
        //    {
        //        using (CsvFileReader reader = new CsvFileReader(@"H:\Output\time_series_covid19_deaths_global_iso3_regions.csv"))
        //        {
        //            int iter = 0;

        //            CsvRow row = new CsvRow();
        //            while (reader.ReadRow(row))
        //            {
        //                var newsEntity = new EventsCorona();
        //                newsEntity.Province = row[0];
        //                newsEntity.Country = row[1];
        //                newsEntity.Lat = row[2];
        //                newsEntity.Long = row[3];
        //                newsEntity.EventDate = new List<DateTime>();  

        //                int previous = 0;
        //                int buffer = 4;

        //                var dateTime = new DateTime();
        //                for (int i = buffer; i < row.Count; i++)
        //                {
        //                    var dateRes = FormatDateTime(row[i]);

        //                    if (DateTime.TryParse(dateRes, out dateTime))
        //                    {
        //                        newsEntity.EventDate.Add(dateTime);
        //                    }
        //                    else if (iter > 1 && i < (collect.FirstOrDefault().EventDate.Count) + buffer)
        //                    {
        //                        try
        //                        {
        //                            var eventCoro = new EventsCorona();
        //                            eventCoro.Province = row[0];
        //                            eventCoro.Country = row[1];
        //                            eventCoro.Lat = row[2];
        //                            eventCoro.Long = row[3];

        //                            if (row.Count == 86)
        //                            {
        //                                eventCoro.ISO = row[row.Count - 7];
        //                                eventCoro.Region = row[row.Count - 5];
        //                                eventCoro.SubRegion = row[row.Count - 3];
        //                            }
        //                            else if (row.Count == 85)
        //                            {
        //                                eventCoro.ISO = row[row.Count - 6];
        //                                eventCoro.Region = row[row.Count - 4];
        //                                eventCoro.SubRegion = row[row.Count - 2];
        //                            }

        //                            eventCoro.EventDate = new List<DateTime>();

        //                            var current = Convert.ToInt32(row[i]);
        //                            eventCoro.Deaths = current - previous;

        //                            eventCoro.DeathsTillDate = current;
        //                            eventCoro.EventDate.Add(collect.FirstOrDefault().EventDate[i - buffer]);

        //                            collectEvent.Add(eventCoro);

        //                            previous = Convert.ToInt32(row[i]);
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            var data = ex.ToString();
        //                        }

        //                    }
        //                }

        //                newsEntity.ISO = row[row.Count - 7];
        //                newsEntity.Region = row[row.Count - 5];
        //                newsEntity.SubRegion = row[row.Count - 3];

        //                collect.Add(newsEntity);

        //                iter++;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var data = ex.ToString();
        //    }

        //    //write out to file or db

        //    WriteToCSVFile(collectEvent);


        //    return collectEvent;
        //}

        public string FormatDateTime(string date)
        {
            string dateValue = "";

            var split = date.Split('/');

            if (split.Length > 1)
            {
                if ((split.FirstOrDefault() == "1") ||
                    (split.FirstOrDefault() == "2") ||
                    (split.FirstOrDefault() == "3") ||
                    (split.FirstOrDefault() == "4") ||
                    (split.FirstOrDefault() == "5") ||
                    (split.FirstOrDefault() == "6") ||
                    (split.FirstOrDefault() == "7") ||
                    (split.FirstOrDefault() == "8") ||
                    (split.FirstOrDefault() == "9"))
                {
                    dateValue = split[1] + "/" + "0" + split.FirstOrDefault() +  "/" + split[2] + "20";
                }
            }
            else
            {
                return date;
            }

            return dateValue;
        }

        //public void WriteToCSVFile(List<EventsCorona> groupedBySimilarity)
        //{
        //    var directoryOutput = System.Configuration.ConfigurationManager.AppSettings["DIAGNOSTIC_OUTPUT"];

        //    if (Directory.Exists(directoryOutput) == false)
        //    {
        //        Directory.CreateDirectory(directoryOutput);
        //    }

        //    String dateStr = String.Format("{0}{1}{2}_{3}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour);

        //    String tempFileName = "Corona" + dateStr + "_";
        //    String fileName = tempFileName + ".csv";

        //    String fullPath = directoryOutput + "\\" + fileName;
        //    if (!System.IO.File.Exists(fullPath))
        //    {
        //        //crude approach
        //        using (System.IO.FileStream fs = System.IO.File.Create(fullPath)) { }
        //    }

        //    try
        //    {
        //        using (CsvFileWriter writer = new CsvFileWriter(fullPath))
        //        {
        //            int iter = 0;


        //            foreach (var groupd in groupedBySimilarity) //result.Items
        //            {
        //                CsvRow csvRow = new CsvRow();

        //                if (iter == 0)
        //                {
        //                    csvRow.Add("Province");
        //                    csvRow.Add("Country");
        //                    csvRow.Add("Region");
        //                    csvRow.Add("SubRegion");
        //                    csvRow.Add("Latitude");
        //                    csvRow.Add("Longitude");
        //                    csvRow.Add("Deaths");
        //                    csvRow.Add("DeathsToDate");
        //                    csvRow.Add("Date");
        //                }

        //                else
        //                {

        //                    if (!string.IsNullOrEmpty(groupd.Province))
        //                    {
        //                        csvRow.Add(groupd.Province);
        //                    }
        //                    else
        //                    {
        //                        csvRow.Add(" ");
        //                    }

        //                    if (!string.IsNullOrEmpty(groupd.Country))
        //                    {
        //                        csvRow.Add(groupd.Country);
        //                    }
        //                    else
        //                    {
        //                        csvRow.Add(" ");
        //                    }

        //                    if (!string.IsNullOrEmpty(groupd.Region))
        //                    {
        //                        csvRow.Add(groupd.Region);
        //                    }
        //                    else
        //                    {
        //                        csvRow.Add(" ");
        //                    }

        //                    if (!string.IsNullOrEmpty(groupd.SubRegion))
        //                    {
        //                        csvRow.Add(groupd.SubRegion);
        //                    }
        //                    else
        //                    {
        //                        csvRow.Add(" ");
        //                    }

        //                    if (!string.IsNullOrEmpty(groupd.Lat))
        //                    {
        //                        csvRow.Add(groupd.Lat);
        //                    }
        //                    else
        //                    {
        //                        csvRow.Add(" ");
        //                    }

        //                    if (!string.IsNullOrEmpty(groupd.Long))
        //                    {
        //                        csvRow.Add(groupd.Long);
        //                    }
        //                    else
        //                    {
        //                        csvRow.Add(" ");
        //                    }

        //                    csvRow.Add(groupd.Deaths.ToString());
        //                    csvRow.Add(groupd.DeathsTillDate.ToString());
        //                    csvRow.Add(groupd.EventDate.FirstOrDefault().ToString("yyyy-MM-dd"));
        //                }

        //                writer.WriteRow(csvRow);
                        
        //                iter++;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Console.WriteLine(ex.ToString());
        //    }
        //}


        //private List<NewsEnity> Diagnostic_ReadCSVFile()
        //{
        //    var collect = new List<NewsEnity>();

        //    using (CsvFileReader reader = new CsvFileReader(@"C:\TradeRiser\nucleartest.csv"))
        //    //using (CsvFileReader reader = new CsvFileReader(@"C:\TradeRiser\ManualHeadlines.csv"))
        //    //using (CsvFileReader reader = new CsvFileReader(@"C:\TradeRiser\NLPNicknames.csv"))
        //    {
        //        var restClient = new RestClient();

        //        CsvRow row = new CsvRow();
        //        while (reader.ReadRow(row))
        //        {
        //            var newsEntity = new NewsEnity();
        //            newsEntity.Title = row[0];
        //            newsEntity.Url = row[1];
        //            newsEntity.NewsSource = row[2];

        //            var des = restClient.LoadFullDescription(newsEntity.Url);
        //            //var des = restClient.LoadFullDescription("https://www.cnbc.com/2017/08/24/tropical-storm-harvey-heads-for-texas-may-become-hurricane.html"); 

        //            if (des != null)
        //            {
        //                newsEntity.Description = des;
        //                collect.Add(newsEntity);    
        //                Console.WriteLine("Reading :: " + newsEntity.Title);
        //            }
        //        }
        //    }
        //    return collect;
        //}

        private List<NewsEvent> AssignEventGrouping(Dictionary<int, List<NewsEnity>> newsCollection)
        {
            var newsList = new List<NewsEvent>();

            //For the news headline group, nominate a master headline i.e. reuters or bloomberg


            //parse news content and tag groupings

            //come up with new categories....use predictive skipagram



            return newsList;
        }

        private List<CryptoRank> ConvertToNewsEntity(string results, string source)
        {
            dynamic json = Newtonsoft.Json.JsonConvert.DeserializeObject(results);
            var resultsCount = (System.Collections.IList)json["data"];
            var newEntityList = new List<CryptoRank>();
            var restClient = new RestClient();            
            
            //var ten = json["data"][0].name + " " + json["data"][0].symbol;

            if (resultsCount != null)
            {
                var newEntityListConcurrent = new System.Collections.Concurrent.ConcurrentBag<CryptoRank>();
                int[] coll = new int[resultsCount.Count];
                for (int i = 0; i < resultsCount.Count; i++) { coll[i] = i; }

                Parallel.ForEach(coll,
                    i =>
                    {
                        var newEntity = new CryptoRank();

                        newEntity.Name = json["data"][i].name;
                        newEntity.Symbol = json["data"][i].symbol + "USDT";

                        string symLookUp = json["data"][i].symbol;

                        if (_logos.TryGetValue(symLookUp, out var link))
                        {
                            newEntity.Logo = link;
                        }

                        newEntity.MarketCap = Math.Round(Convert.ToDouble(json["data"][i]["quote"]["USD"]["market_cap"]), 2);

                        newEntity.Percentage1h = Math.Round(Convert.ToDouble(json["data"][i]["quote"]["USD"]["percent_change_1h"]), 2);
                        newEntity.Percentage24h = Math.Round(Convert.ToDouble(json["data"][i]["quote"]["USD"]["percent_change_24h"]), 2);

                        newEntity.Percentage7d = Math.Round(Convert.ToDouble(json["data"][i]["quote"]["USD"]["percent_change_7d"]), 2);
                        newEntity.Price = Math.Round(Convert.ToDouble(json["data"][i]["quote"]["USD"]["price"]), 4);

                        newEntity.CirculatingSupply = Convert.ToDouble(json["data"][i]["circulating_supply"]);
                        newEntity.Volume = Math.Round(Convert.ToDouble(json["data"][i]["quote"]["USD"]["volume_24h"]), 2);

                        newEntityListConcurrent.Add(newEntity);
                    });

                return newEntityListConcurrent.ToList();
            }
            return newEntityList;
        }



        private void NewsListings()
        {
            using (StreamReader sr = new StreamReader(System.Configuration.ConfigurationManager.AppSettings["NEWS_AGENCIES"].ToString()))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (String.IsNullOrEmpty(line) == false)
                    {

                        try
                        {
                            var array = line.Split(',');

                            _logos.Add(array[0], array[1]);
                        }
                        catch (Exception ex)
                        {
                            string op = ex.ToString();
                        }
                    }
                }
            }
        }
    }
}
