﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoRankingConsoleApp
{

    public class CustomerData
    {
        public String Name { get; set; }
        public String Firstname { get; set; } 
        public String Secondname { get; set; } 
        public String Role { get; set; }
        public String Email { get; set; }
        public String Status { get; set; }
        public String Website { get; set; }
        public String LinkedIn { get; set; }
        public String Company { get; set; }
        public String OriginalPage { get; set; }
        public String FullPageNoTags { get; set; }
        public String SearchQuery { get; set; }
    }

    public class WebData
    {
        public string Website;
        public string PreferredEmailFormat;
        public List<CustomerData> ListCustomerData = new List<CustomerData>();
    }


    public class EconomicData
    {
        public String EventHeadline { get; set; }
        public String CategoryType { get; set; } //Economic Activity, Employment, Central Bank
        public String EventType { get; set; } //PMI, MoM, FOMC, MPC
        public DateTime ReleaseDateTime { get; set; }
        public String Country { get; set; }
        public String DescriptionOfEvent { get; set; }
        public String Importance { get; set; }
        public String InstitutionBody { get; set; }
        public String EventInfluentialPersonnelFirstName { get; set; }
        public String EventInfluentialPersonnelLastName { get; set; }

        public String EconomicSource { get; set; }
        public String EconomicSourceTitle { get; set; }
        public String Currency { get; set; }

        public String CurrentActualValue { get; set; }
        public String CurrentForecastValue { get; set; }
        public String CurrentPreviousValue { get; set; }
        public String NumericType { get; set; }


        public String JSONSrc { get; set; }
    }



    public class EconomicPageTemplate
    {
        //no release date
        public String EventHeadline { get; set; }
        public String CategoryType { get; set; } //Economic Activity, Employment, Central Bank
        public String EventType { get; set; } //PMI, MoM, FOMC, MPC

        public String Country { get; set; }
        public String DescriptionOfEvent { get; set; }
        public String Importance { get; set; }
        public String InstitutionBody { get; set; }
        public String EventInfluentialPersonnelFirstName { get; set; }
        public String EventInfluentialPersonnelLastName { get; set; }

        public String EconomicSource { get; set; }
        public String EconomicSourceTitle { get; set; }
        public String Currency { get; set; }

        public List<EconomicValues> EconomicValuesList = new List<EconomicValues>();
        public String JSONSrc { get; set; }
    }

    public struct EconomicValues
    {
        public DateTime ReleaseDateTime { get; set; }
        public String CurrentActualValue { get; set; }
        public String CurrentForecastValue { get; set; }
        public String CurrentPreviousValue { get; set; }
        public String NumericType { get; set; }
    }
}
