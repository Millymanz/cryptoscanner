﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using System.Net;
using System.Xml.XPath;
using HtmlAgilityPack;

namespace DataOrganizerLibrary
{
    public class RestClient
    {
        public string GetFromRestServiceAlt(string url)
        {


            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;

            try
            {
                var apiUrl = url;
                WebRequest request = WebRequest.Create(apiUrl);
                // Set the Method property of the request to POST.
                request.Method = "GET";


                //request.Headers.Add("X-Mashape-Key", "rAxpHYY11omshnDRGxOiYNpNgjb8p1d8Zt2jsnrCJiSPuH3NPk");

                request.Headers.Add("X-CMC_PRO_API_KEY", "a7b2ffed-91ec-40f1-81aa-4051c0988f47");
                request.Headers.Add("Accepts", "application/json");




               // request.ContentType = "application/json";
                // Get the response.
                WebResponse response = request.GetResponse();
                // Get the stream containing content returned by the server.
                var dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                // Display the content.
                Console.WriteLine(responseFromServer);
                // Clean up the streams.
                reader.Close();
                response.Close();

                return responseFromServer;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return "";
        }


        public string GetFromRestService(string url)
        {

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;

            try
            {
                var apiUrl = url;
                WebRequest request = WebRequest.Create(apiUrl);
                // Set the Method property of the request to POST.
                request.Method = "GET";

                request.Headers.Add("X-CMC_PRO_API_KEY", "a7b2ffed-91ec-40f1-81aa-4051c0988f47");
                request.Headers.Add("Accepts", "application/json");

                request.ContentType = "application/json";
                // Get the response.
                WebResponse response = request.GetResponse();
                // Get the stream containing content returned by the server.
                var dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                // Display the content.
                Console.WriteLine(responseFromServer);
                // Clean up the streams.
                reader.Close();
                response.Close();

                return responseFromServer;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return "";
        }

        public string LoadFullDescription(string descriptionUrl)
        {
            try
            {
                var getHtmlWeb = new HtmlWeb();
                var document = getHtmlWeb.Load(descriptionUrl);

                return RemoveUnwantedTags(document.DocumentNode.InnerHtml);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string LoadDescription(string descriptionUrl)
        {
            try
            {
                var getHtmlWeb = new HtmlWeb();
                var document = getHtmlWeb.Load(descriptionUrl);

                return document.DocumentNode.InnerHtml;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void TagRemoval(HtmlDocument document, string tag)
        {
            var finaltag = "//" + tag;
            var head = document.DocumentNode.SelectNodes(finaltag);
            if (head != null)
            {
                foreach (var item in head)
                {
                    item.RemoveAllChildren();
                    item.RemoveAll();
                    item.Remove();
                }
                var desDD = document.DocumentNode.InnerHtml;
            }
        }

        private string RemoveUnwantedTags(string data)
        {
            if (string.IsNullOrEmpty(data)) return string.Empty;

            var document = new HtmlDocument();
            document.LoadHtml(data);            

            TagRemoval(document, "head");
            TagRemoval(document, "form");
            TagRemoval(document, "script");
            TagRemoval(document, "style");
            TagRemoval(document, "link");
            TagRemoval(document, "noscript");
            TagRemoval(document, "source");
            TagRemoval(document, "a");            
  

            //var acceptableTags = new String[] { "strong", "em", "u" };
            var acceptableTags = new String[] { "" };

            var nodes = new Queue<HtmlNode>(document.DocumentNode.SelectNodes("./*|./text()"));
            while (nodes.Count > 0)
            {
                var node = nodes.Dequeue();
                var parentNode = node.ParentNode;

                if (!acceptableTags.Contains(node.Name) && node.Name != "#text")
                {
                    var childNodes = node.SelectNodes("./*|./text()");

                    if (childNodes != null)
                    {
                        foreach (var child in childNodes)
                        {
                            nodes.Enqueue(child);
                            parentNode.InsertBefore(child, node);
                        }
                    }
                    parentNode.RemoveChild(node);
                }
            }
            var finalText = document.DocumentNode.InnerHtml.Trim();

            string result = System.Text.RegularExpressions.Regex.Replace(finalText, @"\r\n?|\n\<!--", " ");
            return result;
        }
    }
}
