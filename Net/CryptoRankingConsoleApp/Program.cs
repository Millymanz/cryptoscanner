﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using Mono.Web;
using System.Net;
using DataOrganizerLibrary;
using System.IO;

namespace CryptoRankingConsoleApp
{

    class Program
    {
        //origin

        private static string API_KEY = "a7b2ffed-91ec-40f1-81aa-4051c0988f47";

        private static string DateStartDate = "";

        private static string BasePath = "";

        public static string DateFormat = "dd/MM/yyyy HH:mm";

        static void Main(string[] args)
        {
            EmailTest();

            var currentScrapeDateTime = DateTime.Now;

            BasePath = System.Configuration.ConfigurationManager.AppSettings["BASE_OUTPUT"].ToString() + "\\" + DateTime.Now.ToString("MMMM yyyy")
    + "\\" + DateTime.Now.ToString("dd") + DateTime.Now.ToString("MMMM");


            var min = DateTime.Now.Minute.ToString().Length == 2 ? DateTime.Now.Minute.ToString() : "0" + DateTime.Now.Minute.ToString();
            var hour = DateTime.Now.Hour.ToString().Length == 2 ? DateTime.Now.Hour.ToString() : "0" + DateTime.Now.Hour.ToString();

            DateStartDate = String.Format("{0}{1}{2}_{3}{4}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, hour, min);

            var newsProcess = new NewsProcessor();

            //var something = newsProcess.FetchRanking();
            var something = newsProcess.FetchRankingMain();

            var benchmarkFileDate = String.Format("{0}{1}{2}_{3}{4}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, "00", "00");
            var latest = BasePath + "\\csv\\Ranking_Snapshot_" + benchmarkFileDate + ".csv";

            var differenceStr = "";

            var previousNew = BasePath + "\\csv\\PreviousTopEight.csv";

            var dataMgr = new DataMgr(latest, previousNew);

            dataMgr.WriteToCSVFile(something, BasePath
                    + "\\csv\\Ranking_Snapshot_" + DateStartDate + ".csv", false, true);

            if (File.Exists(latest) && dataMgr.CryptoRanks.Any())
            {
                var difference = something.Select(m => m.Symbol).Except(dataMgr.CryptoRanks.Select(m => m.Symbol));

                var numberTopList = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NEWTOPLIST"].ToString());

                var newInTopEight = something.Take(numberTopList).Where(m => difference.Any(p => p == m.Symbol));

                var finalNewInTopEight = new List<CryptoRank>();

                if (dataMgr.PreviousNewInRanking.Any())
                {
                    finalNewInTopEight = newInTopEight.Where(ko => dataMgr.PreviousNewInRanking.Any(rt => rt.Symbol == ko.Symbol) == false).ToList();
                }
                else
                {
                    finalNewInTopEight = newInTopEight.ToList();
                }
                //only coins with large volumes
                finalNewInTopEight = finalNewInTopEight.Where(mk => mk.Volume > 500000).ToList();

                if (difference.Any())
                {
                    foreach (var item in difference)
                    {
                        differenceStr += item + " ";
                    }
                }

                Console.WriteLine("Total New Cryptos :: " + finalNewInTopEight.Count());

                if (finalNewInTopEight.Any())
                {
                    if (File.Exists(previousNew) == false)
                    {
                        using (File.Create(previousNew)) { };
                        dataMgr.CreateHeaders(previousNew);
                    }

                    dataMgr.WriteToCSVFile(finalNewInTopEight.ToList(), previousNew, false, false);

                    dataMgr.LogAvailableTrade(finalNewInTopEight.ToList(), something, currentScrapeDateTime);

                    //Email or notify
                    EmailNotify(finalNewInTopEight.ToList(), something);
                    Console.WriteLine("Email Sent");
                }
            }

            var tableStr = "";

            for (int i = 0; i < something.Count; i++)
            {
                var no = i + 1;
                tableStr += "<tr>";
                tableStr += "<td>" + no + "</td>";
                tableStr += "<td><img style='width: 24px; height: 24px;' src='" + something[i].Logo + "'>   " + something[i].Symbol + "</td>";
                tableStr += "<td>" + something[i].Name + "</td>";
                tableStr += "<td>" + something[i].Price + "</td>";
                tableStr += something[i].Percentage24h > 0 ? "<td><span style='color: green; padding: 0; border - radius:8px'>" + something[i].Percentage24h + "</span></td>"
                    : "<td><span style='color: red; padding: 0; border - radius:8px'>" + something[i].Percentage24h + "</span></td>";
                tableStr += "<td>" + something[i].Percentage7d + "</td>";
                tableStr += "<td>" + something[i].MarketCap + "</td>";
                tableStr += "<td>" + something[i].Volume + "</td>";
                tableStr += "<td>" + something[i].CirculatingSupply + "</td>";
                tableStr += "</tr>";
            }

            var time = DateTime.Now.ToString();

            var source = @"  
                <!DOCTYPE html>  
                    <html>  
                        <head>  
                            <style>  
                                table {  
                                  font-family: arial, sans-serif;  
                                  border-collapse: collapse;  
                                  width: 100%;  
                                }  
                                  
                                td, th {  
                                  border: 1px solid #dddddd;  
                                  text-align: left;  
                                  padding: 10px;  
                                  font-size: 22px;  
                                }  
                                  
                                tr:nth-child(even) {  
                                  background-color: #dddddd;  
                                }  
                               .coin-logo {
                                    height: 24px;
                                    width: 24px;
                                    border-radius: 12px;
                                }                                                         
                            </style>  
                         </head>  
                    <body>                        
                        <h2>Cryptocurrency Rankings " + time + "</h2>" +
                        "<table>" +
                          "<tr>" +
                            "<th>Rank</th>" +
                            "<th>Symbol</th>" +
                            "<th>Name</th>" +
                            "<th>Price</th>" +
                            "<th>24h%</th>" +
                            "<th>7d%</th>" +
                            "<th>Market Cap</th>" +
                            "<th>Volume (24h)</th>" +
                            "<th>CirculatingSupply</th>" +
                          "</tr>"
                             + tableStr +
                        "</table>" +
                        "<br/><br/> <div style='font-family: arial, sans-serif; font-size: 22px; '> Left Top 3 :: " + differenceStr + "</div>" +
                         "</body></html>";

            StartBrowser(source);
        }

        private static void EmailNotify(List<CryptoRank> topEight, List<CryptoRank> rankingList)
        {
            var message = new StringBuilder();

            foreach (var pr in topEight)
            {
                var expire = DateTime.Now.AddMinutes(12);

                var hold = DateTime.Now.AddHours(13);

                var rank = rankingList.FindIndex(a => a.Symbol == pr.Symbol);
                rank = rank + 1;

                message.AppendLine("\n\n ");
                message.AppendLine("<b>" + pr.Symbol + "</b>");
                message.AppendLine("<br>");
                message.AppendLine("Expiry Time :: " + expire.ToString("HH: mm:ss"));
                message.AppendLine("<br>");
                message.AppendLine("Maximum Hold :: " + hold.ToString(DateFormat));
                message.AppendLine("<br>");
                message.AppendLine("<b>Rank Breakthrough Position :: " + rank + "</b>");
                message.AppendLine("<br><br>");
                message.AppendLine("\n");
            }

            message.AppendLine("Date :" + DateTime.Now.ToString(DateFormat));
            message.AppendLine("Newest Crypto In the Top 8\n");

            new EmailManager().Send(null, "Latest In Rank :: Top 6 " + DateTime.Now.ToString(DateFormat), message.ToString());
        }


        private static void StartBrowser(string source)
        {
            var th = new Thread(() =>
            {
                var webBrowser = new WebBrowser();
                webBrowser.ScrollBarsEnabled = false;
                webBrowser.IsWebBrowserContextMenuEnabled = true;
                webBrowser.AllowNavigation = true;
                webBrowser.Width = 1918;
                webBrowser.Height = 1250;

                webBrowser.DocumentCompleted += webBrowser_DocumentCompleted;
                webBrowser.DocumentText = source;

                Application.Run();
            });
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }

        static void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            var outputRank = BasePath;

            var webBrowser = (WebBrowser)sender;
            using (Bitmap bitmap =
                new Bitmap(
                    webBrowser.Width,
                    webBrowser.Height))

            {

                webBrowser
                    .DrawToBitmap(
                    bitmap,
                    new System.Drawing
                         .Rectangle(0, 0, bitmap.Width, bitmap.Height));
                bitmap.Save(outputRank + @"\\ranking\\Ranking_Snapshot_" + DateStartDate + ".jpg",
                    System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            Environment.Exit(0);

        }

        private static void EmailTest()
        {
            var testBool = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EMAIL_TEST"].ToString());

            if (testBool)
            {
                var finalNewInTopEight = new List<CryptoRank>();
                var cryptoRank = new CryptoRank();
                cryptoRank.Symbol = "FAKECOIN";
                cryptoRank.Name = "Tester Coin";
                cryptoRank.Price = 45.58;
                cryptoRank.Percentage1h = 1.2;
                cryptoRank.Percentage24h = 51.3;
                cryptoRank.MarketCap = 5451455;
                finalNewInTopEight.Add(cryptoRank);

                EmailNotify(finalNewInTopEight, new List<CryptoRank>());
            }
        }

        static string makeAPICall()
        {
            var URL = new UriBuilder("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest");

            var queryString = HttpUtility.ParseQueryString(string.Empty);
            queryString["start"] = "1";
            queryString["limit"] = "5000";
            queryString["convert"] = "USD";

            URL.Query = queryString.ToString();

            var client = new WebClient();
            client.Headers.Add("X-CMC_PRO_API_KEY", API_KEY);
            client.Headers.Add("Accepts", "application/json");
            return client.DownloadString(URL.ToString());

        }
    }
}
