﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoRankingConsoleApp
{
    public class CryptoRank
    {
        public string Name { get; set; }
        public string Symbol { get; set; }
        public string Logo { get; set; }
        public double Price { get; set; }
        public double Percentage1h { get; set; }
        public double Percentage24h { get; set; }
        public double Percentage7d { get; set; }
        public double MarketCap { get; set; }
        public double Volume { get; set; }
        public double CirculatingSupply { get; set; }

        public CryptoRank()
        {
            Price = 0.0;
            Percentage1h = 0.0;
            Percentage24h = 0.0;
            Percentage7d = 0.0;
            MarketCap = 0.0;
            Volume = 0.0;
            CirculatingSupply = 0.0;
    }
}
}
