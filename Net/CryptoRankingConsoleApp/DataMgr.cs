﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;


namespace CryptoRankingConsoleApp
{
    public class DataMgr
    {
        private static object locker = new object();

        public List<CryptoRank> CryptoRanks { get; private set; }

        public List<CryptoRank> PreviousNewInRanking { get; private set; }
        public DataMgr(string path, string previousNew)
        {
            PreviousNewInRanking = new List<CryptoRank>();
            CryptoRanks = new List<CryptoRank>();

            if (File.Exists(path))
            {
                using (var reader = new StreamReader(path))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    CryptoRanks = csv.GetRecords<CryptoRank>().ToList();
                }
            }

            if (File.Exists(previousNew))
            {
                using (var reader = new StreamReader(previousNew))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    PreviousNewInRanking = csv.GetRecords<CryptoRank>().ToList();
                }
            }
        }

        public static void LogActivity(string msg)
        {
            using (StreamWriter sw = File.CreateText("appLog.txt"))
            {
                sw.WriteLine(msg);
            }
        }

        public void CreateHeaders(string path)
        {
            lock (locker)
            {
                var fileName = path;

                try
                {
                    using (CsvFileWriter writer = new CsvFileWriter(fileName))
                    {
                        var csvHeaderRow = new CsvRow();
                        csvHeaderRow.Add("Symbol");
                        csvHeaderRow.Add("Name");
                        csvHeaderRow.Add("Price");
                        csvHeaderRow.Add("Percentage24h");
                        csvHeaderRow.Add("Percentage7d");
                        csvHeaderRow.Add("MarketCap");
                        csvHeaderRow.Add("Volume");
                        csvHeaderRow.Add("CirculatingSupply");
                        csvHeaderRow.Add("Logo");
                        csvHeaderRow.Add("Percentage1h");
                        writer.WriteRow(csvHeaderRow);

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        public static string RoundToNearestFiveMin(DateTime dateItem)
        {
            var reminder = dateItem.Minute % 5;
            var minutes = dateItem.Minute - reminder;

            var tempDatetime = new DateTime(dateItem.Year, dateItem.Month, dateItem.Day, dateItem.Hour, minutes, 0);

            return tempDatetime.ToString("yyyy-MM-dd HH:mm");
        }


        public void LogAvailableTrade(List<CryptoRank> cryptoList, List<CryptoRank> rankingList, DateTime scrapeDateTime)
        {
            var settingsList = new List<string>()
            {
               System.Configuration.ConfigurationManager.ConnectionStrings["LOG_TRADE_OPP"].ToString(),
               System.Configuration.ConfigurationManager.ConnectionStrings["LOG_TRADE_OPP_2ND"].ToString(),
            };

            foreach (var settings in settingsList)
            {
                bool notInDB = true;
                int counter = 1;

                while (notInDB)
                {
                    string storedProc = "proc_InsertAvailableTrades";

                    try
                    {

                        foreach (var itemCrypto in cryptoList)
                        {
                            var rank = rankingList.FindIndex(a => a.Symbol == itemCrypto.Symbol);
                            rank = rank + 1;

                            using (SqlConnection conn = new SqlConnection(settings.ToString()))
                            {
                                var datetime = RoundToNearestFiveMin(scrapeDateTime);
                                var expDatetime = RoundToNearestFiveMin(scrapeDateTime.AddMinutes(15));

                                conn.Open();
                                SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                                sqlCommand.CommandType = CommandType.StoredProcedure;

                                sqlCommand.Parameters.AddWithValue("@Rank", rank);
                                sqlCommand.Parameters.AddWithValue("@DateTime", datetime);
                                sqlCommand.Parameters.AddWithValue("@Symbol", itemCrypto.Symbol);
                                sqlCommand.Parameters.AddWithValue("@ExpiryDateTime", expDatetime);

                                sqlCommand.CommandTimeout = 0;
                                sqlCommand.ExecuteNonQuery();

                                notInDB = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (counter == 5)
                        {
                            Console.WriteLine(ex.ToString());

                            var message = new StringBuilder();
                            message.AppendLine("Failed To Insert In DB Investigate For :- " +
                                               cryptoList.FirstOrDefault().Symbol);
                            message.AppendLine("");
                            message.AppendLine(ex.ToString());

                            new EmailManager().Send(null, "Available Trade Entry Failed", message.ToString());
                        }

                    }

                    counter++;
                }
            }
        }


        public void WriteToCSVFile(List<CryptoRank> cryptoRanks, string path, bool deleteIfExist, bool addHeaders)
        {
            lock (locker)
            {
                var fileName = path;

                try
                {
                    if (File.Exists(path) && deleteIfExist)
                    {
                        File.Delete(path);
                    }

                    using (CsvFileWriter writer = new CsvFileWriter(fileName))
                    {
                        var csvHeaderRow = new CsvRow();

                        if ((deleteIfExist == false && addHeaders))
                        {
                            csvHeaderRow.Add("Symbol");
                            csvHeaderRow.Add("Name");
                            csvHeaderRow.Add("Price");
                            csvHeaderRow.Add("Percentage24h");
                            csvHeaderRow.Add("Percentage7d");
                            csvHeaderRow.Add("MarketCap");
                            csvHeaderRow.Add("Volume");
                            csvHeaderRow.Add("CirculatingSupply");
                            csvHeaderRow.Add("Logo");
                            csvHeaderRow.Add("Percentage1h");
                            writer.WriteRow(csvHeaderRow);
                        }

                        foreach (var rowitem in cryptoRanks)
                        {
                            CsvRow csvRow = new CsvRow();

                            if (String.IsNullOrEmpty(rowitem.Symbol) == false)
                            {
                                csvRow.Add(rowitem.Symbol.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }

                            if (String.IsNullOrEmpty(rowitem.Name) == false)
                            {
                                csvRow.Add(rowitem.Name.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }

                            csvRow.Add(rowitem.Price.ToString());
                            csvRow.Add(rowitem.Percentage24h.ToString());
                            csvRow.Add(rowitem.Percentage7d.ToString());
                            csvRow.Add(rowitem.MarketCap.ToString());
                            csvRow.Add(rowitem.Volume.ToString());
                            csvRow.Add(rowitem.CirculatingSupply.ToString());

                            if (String.IsNullOrEmpty(rowitem.Logo) == false)
                            {
                                csvRow.Add(rowitem.Logo.ToString());
                            }
                            else
                            {
                                csvRow.Add("null");
                            }

                            csvRow.Add(rowitem.Percentage1h.ToString());

                            writer.WriteRow(csvRow);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Logger.log.Error(ex);
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
